import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/patient-api";
import * as API_USERS_M from "../../medication/api/medication-api";


class PatientTable extends React.Component {


    constructor(props) {
        super(props);
        this.reloadHandler = this.props.reloadHandler;
        this.valid = false;
        this.state = {
            dataMed : [],
            tableData: this.props.tableData
        };
        this.getMeds = this.getMeds.bind(this);
    }

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Username',
            accessor: 'username',
        },
        {
            Header: 'Password',
            accessor: 'password',
        },
        {
            Header: 'MedicalRecord',
            accessor: 'medicalRecord',
        },
        {
            Header: 'Delete',
            accessor: 'id',
            Cell:cell=>(<button className="deleteButton" onClick={()=> this.handleDelete(cell.value)}> Delete </button>)
        },
        {
            Header: 'Update',
            accessor: 'id',
            Cell:cell=>(<button className="updateButton" onClick={()=> this.handleById(cell.value)}> Update </button>)
        },
        {
            Header: 'Medical Plan',
            accessor: 'id',
            Cell:cell=>(<button className="updateButton" onClick={()=> this.handleMedical(cell.value)}> Add Medical Plan </button>)
        }
    ];

    filters = [
        {
            accessor: 'name',
        }
    ];

    componentDidMount() {
        this.getMeds();
    }

    handleMedical(id) {

        this.getMeds();
        let a = [id, this.state.dataMed];
        this.props.info(a);
        return a;
    }

    handleById(id) {
        this.props.idValue(id);

        return id;
    }

    handleDelete(id) {
        return API_USERS.deletePatientById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted patient with id: " + result);
                 this.reloadHandler();
            }
        });

    }

   getMeds() {
        return API_USERS_M.getMedication((result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get patient with id: " + result);

                this.setState(() => ({
                    dataMed : result,
                }))


            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleUpdateById(id) {
        return API_USERS.getPatientById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get patient with id: " + id);
                this.reloadHandler();
            }
        });
    }


    render() {
        return (
            <Table

                data={this.state.tableData}
                columns={this.columns}
                search={this.filters}
                pageSize={5}
                onChange={this.handleById}

            />
        )
    }
}

export default PatientTable;