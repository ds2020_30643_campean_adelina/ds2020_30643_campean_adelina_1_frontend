import React from 'react';
import validate from "./validators/patient-validators";
import { FormGroup, Input, Label,Button } from 'reactstrap';

import * as API_USERS from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import moment from "moment";


class PatientUpdateForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.id = this.props.id;
        this.dataInfo = this.props.dataInfo;
        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                address: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                birthdate: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },
                gender: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 1
                    }
                },
                username: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                password: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                medicalRecord: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                }

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.editPatient = this.editPatient.bind(this);
    }

    toggleForm() {

        this.setState({collapseForm: !this.state.collapseForm});

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    editPatient(patient) {
        return API_USERS.updatePatient(patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated patient with id: " + result);

                this.reloadHandler();

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {

        let date = moment(this.state.formControls.birthdate.value).format('MM.DD.YYYY');

        let patient = {
            id: this.id,
            name: this.state.formControls.name.value,
            address: this.state.formControls.address.value,
            birthdate: date,
            gender: this.state.formControls.gender.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            medicalRecord: this.state.formControls.medicalRecord.value,
            medicationPlans : []
        };

        console.log(patient);
        this.editPatient(patient);
    }

    render() {

        return (
            <div>
                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.dataInfo[1]}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           //valid={this.state.formControls.name.valid}
                           //required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.dataInfo[2]}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.address.touched? 1 : 0}
                          // valid={this.state.formControls.address.valid}
                            required
                    />
                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label for='birthdateField'> Birthdate: </Label>
                    <Input name='birthdate' id='birthdateField' placeholder={this.dataInfo[3]}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.birthdate.touched? 1 : 0}
                          // valid={this.state.formControls.birthdate.valid}
                           //required
                    />
                    {this.state.formControls.birthdate.touched && !this.state.formControls.birthdate.valid &&
                    <div className={"error-message"}> * Birthdate must have a valid format</div>}
                </FormGroup>


                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.dataInfo[4]}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           //valid={this.state.formControls.gender.valid}
                          // required
                    />
                </FormGroup>



                <FormGroup id='medicalRecord'>
                    <Label for='medicalRecordField'> Medical Record: </Label>
                    <Input name='medicalRecord' id='medicalRecordField' placeholder={this.dataInfo[7]}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.medicalRecord.touched? 1 : 0}
                           //valid={this.state.formControls.medicalRecord.valid}
                           //required
                    />
                </FormGroup>

                <FormGroup id='username'>
                    <Label for='usernameField'> Username: </Label>
                    <Input name='username' id='usernameField' placeholder={this.dataInfo[5]}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.username.touched? 1 : 0}
                        //valid={this.state.formControls.username.valid}
                        // required
                    />
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.dataInfo[6]}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.password.touched? 1 : 0}
                        //valid={this.state.formControls.password.valid}
                        // required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} /*disabled={!this.state.formIsValid}*/ onClick={this.handleSubmit }>  Update </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default PatientUpdateForm;
