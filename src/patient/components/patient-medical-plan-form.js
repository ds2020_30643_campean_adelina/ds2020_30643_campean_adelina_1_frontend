import React from 'react';
import validate from "./validators/patient-validators";
import { FormGroup, Input, Label,Button } from 'reactstrap';

import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/patient-api";
import * as API_USERS_M from "../../medication/api/medication-api";
import moment from "moment";

class PatientMedicalPlanForm extends React.Component {

    dataPatient = [];
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.id = this.props.id;
        this.dataInfo = this.props.dataInfo;
        this.medication ={
            id:null,
            name: null,
            sideEffects: null,
            dosage: null
        }
        this.state = {

            dataMed: this.props.dataMed,
            selected: {},
            selectAll:0,
            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                intakeIntervals: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                treatmentPeriod: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                medicationList: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },


            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleRow = this.toggleRow.bind(this);

    }
/*
    toggleRow(title) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[title] = !this.state.selected[title];
        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }*/

    getMedName(name) {
        return API_USERS_M.getMedicationByName(name, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get patient with id: " + name);
                this.medication.id = result.id;
                this.medication.name = result.name;
                this.medication.sideEffects = result.sideEffects;
                this.medication.dosage = result.dosage;

                console.log("med id: "+ this.medication.id);
            }
        });
    }

    toggleRow(name) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[name] = !this.state.selected[name];

        console.log("info "+ name);

        this.getMedName(name);
        this.getPatientById(this.id);

        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll() {
        let newSelected = {};

        if (this.state.selectAll === 0) {
            this.state.dataMed.forEach(x => {
                newSelected[x.name] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: this.state.selectAll === 0 ? 1 : 0
        });
    }

    ifChecked(e) {
        console.log('Checkbox checked:', (e.target.checked));
    }

    columns = [
        {
          Header: '',
            columns: [
                {
                    id: "checkbox",
                    accessor: "",
                    Cell: ({ original }) => {
                        return (
                            <input
                                type="checkbox"
                                className="checkbox"
                                checked={this.state.selected[original.name] === true}
                                onChange={() => this.toggleRow(original.name)}
                            />
                        );
                    },
                    Header: x => {
                        return (
                            <input
                                type="checkbox"
                                className="checkbox"
                                checked={this.state.selectAll === 1}
                                ref={input => {
                                    if (input) {
                                        input.indeterminate = this.state.selectAll === 2;
                                    }
                                }}
                                onChange={() => this.toggleSelectAll()}
                            />
                        );
                    },
                    sortable: false,
                    width: 45
                }]
        },
        {
            Header: 'Name',
            accessor: 'name',
            width: 250

            },
        {
            Header: 'Side Effects',
            accessor: 'sideEffects',
            width: 250
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
            width: 250
        }
    ];

    filters = [

    ];

    toggleForm() {

        this.setState({collapseForm: !this.state.collapseForm});

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    getPatientById(id) {
        return API_USERS.getPatientById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get patient with id: " + id);
                this.dataPatient[0] = result.id;
                this.dataPatient[1] = result.name;
                this.dataPatient[2] = result.address;
                this.dataPatient[3] = result.birthdate;
                this.dataPatient[4] = result.gender;
                this.dataPatient[5] = result.username;
                this.dataPatient[6] = result.password;
                this.dataPatient[7] = result.medicalRecord;
                this.dataPatient[8] = result.medicationPlans;


            }
            else console.log("failed with id: "+ id);
        });
    }

    editPatient(patient) {
        return API_USERS.updatePatient(patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated patient with id: " + result);

                this.reloadHandler();

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }


    handleSubmit() {

        let date = moment(this.dataPatient[3]).format('MM.DD.YYYY');

        let medicationPlans = {

            intakeIntervals: this.state.formControls.intakeIntervals.value,
            treatmentPeriod: this.state.formControls.treatmentPeriod.value,
            medicationList: [this.medication]

        };

        let patient = {
            id: this.id,
            name:  this.dataPatient[1],
            address:  this.dataPatient[2],
            birthdate: date,
            gender: this.dataPatient[4],
            username: this.dataPatient[5],
            password: this.dataPatient[6],
            medicalRecord: this.dataPatient[7],
            medicationPlans: [medicationPlans]
        };


        //console.log("here -  medication plans: " + patient.medicationPlans.medicationList[0].name );
        this.editPatient(patient);
    }

    render() {

        return (
            <div>
                <FormGroup id='intakeIntervals'>
                    <Label for='intakeIntervalsField'> Intake Intervals: </Label>
                    <Input name='intakeIntervals' id='intakeIntervalsField' placeholder={"Enter an interval"}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.intakeIntervals.touched? 1 : 0}
                        //valid={this.state.formControls.name.valid}
                        //required
                    />

                </FormGroup>

                <FormGroup id='treatmentPeriod'>
                    <Label for='treatmentPeriodField'> Treatment Period: </Label>
                    <Input name='treatmentPeriod' id='treatmentPeriodField' placeholder={"Enter a period of treatment"}
                           onChange={this.handleChange}
                           defaultValue={''}
                           touched={this.state.formControls.treatmentPeriod.touched? 1 : 0}
                        // valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <div className="tableWeirdo">
                <Row >
                    <Col sm={{size: '9', offset: 0}}>
                    <Table
                           data={this.state.dataMed}
                           columns={this.columns}
                           search={this.filters}
                           pageSize={5}

                    />
                    </Col>
                </Row>
                </div>
                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} /*disabled={!this.state.formIsValid}*/ onClick={this.handleSubmit }>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default PatientMedicalPlanForm;
