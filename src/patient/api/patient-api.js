import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function getPatientById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + "/" + params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientByUsername(params, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + "/find-user/"+ params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function deletePatientById(params, callback) {
    if(window.confirm('Are you sure?')) {
        let request = new Request(HOST.backend_api + endpoint.patient + "/" + params, {
            method: 'DELETE',

        });

        console.log("URL: " + request.url);
        RestApiClient.performRequest(request, callback);
    }
}

function insertPatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.patient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function updatePatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.patient , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getPatients,
    getPatientById,
    getPatientByUsername,
    deletePatientById,
    updatePatient,
    insertPatient
};
