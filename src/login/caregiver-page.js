import React from 'react';


import {
    Button, Col,
    Input, Row
} from 'reactstrap';

import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Redirect} from "react-router-dom";
import Table from "../commons/tables/table";


class CaregiverPage extends React.Component {


    constructor(props) {
        super(props);
        this.value = props;
        this.caregiver = {
/*
            name: props.location.state.caregiver.name,
            birthdate: props.location.state.caregiver.birthdate,
            gender: props.location.state.caregiver.gender,
            address: props.location.state.caregiver.address,
            username: props.location.state.caregiver.username,
            password: props.location.state.caregiver.password,
            listOfPatients: props.location.state.caregiver.listOfPatients*/
            name:null,
            birthdate: null,
            gender: null,
            address: null,
            username: null,
            password: null,
            listOfPatients: null
        }

        console.log("caregiver first list : "+ this.caregiver.listOfPatients);


    }
    // this.handleSubmit = this.handleSubmit.bind(this);

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Username',
            accessor: 'username',
        },
        {
            Header: 'Password',
            accessor: 'password',
        },
        {
            Header: 'MedicalRecord',
            accessor: 'medicalRecord',
        }];
    filters = [
        {
            accessor: 'name',
        }
    ];
    render() {
        if(localStorage.getItem('account') ==="caregiver") {

            this.caregiver.name = this.value.location.state.caregiver.name;
            this.caregiver.birthdate= this.value.location.state.caregiver.birthdate;
            this.caregiver.gender= this.value.location.state.caregiver.gender;
            this.caregiver.address= this.value.location.state.caregiver.address;
            this.caregiver.username= this.value.location.state.caregiver.username;
            this.caregiver.password= this.value.location.state.caregiver.password;
            this.caregiver. listOfPatients= this.value.location.state.caregiver.listOfPatients;

            return (
                <div>

                    <div className="logpatient">
                        <h1 className="pinfo">Caregiver Information </h1>

                        <div className="logout-patient">

                            <a href="/login">LOGOUT</a>
                        </div>
                    </div>

                    <div className="tableClass">
                        <Table className="tableDetails"
                               data={this.caregiver.listOfPatients}
                               columns={this.columns}
                               search={this.filters}
                               pageSize={5}
                        />

                    </div>

                </div>

            )
        }else {

            return (<Redirect to={{
                pathname: '/login'}}/>);
        }
    }


}

export default CaregiverPage;