import React from 'react';
import * as API_USERS_A from "../login/api/account-api";

import {
    Button,
    Input
} from 'reactstrap';

import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Redirect} from "react-router-dom";
import Table from "../commons/tables/table";
import * as API_USERS from "../patient/api/patient-api";


class PatientPage extends React.Component {

dataInfo=[];
    constructor(props) {
        super(props);
        this.value = props;
        this.state = {
            value: false
        }
        this.patient = {
            name: null,
            birthdate: null,
            gender: null,
            address: null,
            medicalRecord: null,
            username: null,
            password: null,
            medicationPlans: null
/*
            name: props.location.state.patient.name,
            birthdate: props.location.state.patient.birthdate,
            gender: props.location.state.patient.gender,
            address: props.location.state.patient.address,
            medicalRecord: props.location.state.patient.medicalRecord,
            username: props.location.state.patient.username,
            password: props.location.state.patient.password,
            medicationPlans: props.location.state.patient.medicationPlans

 */
        }

        this.handleMeds = this.handleMeds.bind(this);
    }

    columns = [
        {
            Header: 'intakeIntervals',
            accessor: 'intakeIntervals',
            width: 250
        },
        {
            Header: 'TreatmentPeriod',
            accessor: 'treatmentPeriod',
            width: 250
        },
        {
            Header: 'Medication Plans',
            accessor: 'id',
            width: 250,
            Cell:cell=>(<button className="viewButton" onClick={()=> this.handleMeds(cell.value)}> View Medical Plans </button>)

        }
    ];

    filters = [

    ];

    columns2 = [
        {
            Header: 'Name',
            accessor: 'name',
            width: 250
        },
        {
            Header: 'Side Effects',
            accessor: 'sideEffects',
            width: 250
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
            width: 250
        }
        ];

    handleMeds(med) {
         this.setState(() => ({
            value: false,
        }));
        return API_USERS_A.getMedsMedicalPlan(med, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get med of medical plan with id: " + result);

                this.dataInfo = result;

                this.setState(() => ({
                    value: true,
                }))

                console.log("naaaaameee "+ this.dataInfo[0].name);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    render() {

        const { value } = this.state;

        if(localStorage.getItem('account') === "patient") {

            this.patient.name= this.value.location.state.patient.name;
                this.patient.birthdate= this.value.location.state.patient.birthdate;
                this.patient.gender= this.value.location.state.patient.gender;
                this.patient.address= this.value.location.state.patient.address;
                this.patient.medicalRecord= this.value.location.state.patient.medicalRecord;
                this.patient.username= this.value.location.state.patient.username;
                this.patient.password= this.value.location.state.patient.password;
                this.patient.medicationPlans= this.value.location.state.patient.medicationPlans;

        return (
            <div>

                <div className="logpatient">
                <h1 className="pinfo">Patient Information </h1>

                <div className="logout-patient">

                    <a href="/login">LOGOUT</a>
                </div>
                </div>

                <div className="headerTable">
                    <label>Name </label>
                    <label>Address </label>
                    <label>Birthdate </label>
                    <label>Gender </label>
                    <label>Username </label>
                    <label>Password </label>
                    <label>MedicalRecord</label>
                </div>


                <div className="headerTable">
                    <label >{ this.patient.name }</label>
                    <label>{ this.patient.address }</label>
                    <label>{ this.patient.birthdate }</label>
                    <label>{ this.patient.gender }</label>
                    <label>{ this.patient.username }</label>
                    <label>{ this.patient.password }</label>
                    <label>{ this.patient.medicalRecord }</label>
                </div>
            <br/>
                <h2 className="pinfo2">Medical Plans Table</h2>
                <div className="tableClass">
                    <Table className = "tableDetails"
                           data={this.patient.medicationPlans}
                           columns={this.columns}
                           search={this.filters}
                           pageSize={5}
                    />
                </div>
<br/>
                <h2 className="pinfo2">Medication Table</h2>
                {value && <div className="tableClass">
                    <Table className = "tableDetails"
                           data={this.dataInfo}
                           columns={this.columns2}
                           search={this.filters}
                           pageSize={5}
                           onChange={this.handleMeds}

                    />
                </div> }


            </div>
        )
    }else {

            return (<Redirect to={{
                pathname: '/login'}}/>);
        }
    }


}

export default PatientPage;