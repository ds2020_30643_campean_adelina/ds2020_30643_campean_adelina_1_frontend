import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    login: '/account',
    medicationPlan: '/medication-plan'
};

function getAccountByUsername(params, callback){
    let request = new Request(HOST.backend_api + endpoint.login + "/" + params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function insertAccount(user, callback){
    let request = new Request(HOST.backend_api + endpoint.login , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getMedsMedicalPlan(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + "/" + params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    insertAccount,
    getAccountByUsername,
    getMedsMedicalPlan
};