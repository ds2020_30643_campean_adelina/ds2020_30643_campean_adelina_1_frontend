import React from 'react';
import * as API_USERS from "./api/account-api"
import * as API_USERS_P from "./../patient/api/patient-api"
import * as API_USERS_C from "./../caregiver/api/caregiver-api"

import {
    Button,
    Input
} from 'reactstrap';

import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Redirect} from "react-router-dom";


class Login extends React.Component {

    dataInfo = [];

    constructor(props) {
        super(props);
        this.username = "";
        this.password = "";

       this.state = {
           loginSuccess : false,
           username: '',
           errorStatus: 0,
           error: null,
           patient : {
               id: null,
               name: null,
               birthdate: null,
               gender: null,
               address: null,
               medicalRecord: null,
               username: null,
               password: null,
               medicationPlans: null
           },
            caregiver: {
                name: null,
                birthdate: null,
                gender: null,
                address: null,
                username: null,
                password: null,
                listOfPatients: null
            }

        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getPatient = this.getPatient.bind(this);
        this.getCaregiver = this.getCaregiver.bind(this);

    }


    getPatient(username) {
        return API_USERS_P.getPatientByUsername(username, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get patient with username: " + username);

                this.state.patient.name = result.name;
                this.state.patient.address = result.address;
                this.state.patient.birthdate = result.birthdate;
                this.state.patient.gender = result.gender;
                this.state.patient.username = result.username;
                this.state.patient.password = result.password;
                this.state.patient.medicalRecord = result.medicalRecord;

                this.state.patient.medicationPlans = result.medicationPlans;



/*
                this.state.patient.medicationPlans.intakeIntervals = result.medicationPlans[0].intakeIntervals;
                this.state.patient.medicationPlans.treatmentPeriod = result.medicationPlans[0].treatmentPeriod;
                this.state.patient.medicationPlans.medicationList.name = result.medicationPlans[0].medicationList[0].name;
                this.state.patient.medicationPlans.medicationList.dosage = result.medicationPlans[0].medicationList[0].dosage;
                this.state.patient.medicationPlans.medicationList.sideEffects = result.medicationPlans[0].medicationList[0].sideEffects;*/
                //console.log("heeeeeerE: "+ result.patient.medicationPlans[0].intakeIntervals);

                this.setState(() => ({
                    loginSuccess: true,
                }))
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    getCaregiver(username) {
        return API_USERS_C.getCaregiverByUsername(username, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get caregiver with username: " + username);

                this.state.caregiver.name = result.name;
                this.state.caregiver.address = result.address;
                this.state.caregiver.birthdate = result.birthdate;
                this.state.caregiver.gender = result.gender;
                this.state.caregiver.username = result.username;
                this.state.caregiver.password = result.password;
                this.state.caregiver.listOfPatients = result.listOfPatients;

                console.log("Caregiver lista pac: " + result.listOfPatients);
                this.setState(() => ({
                    loginSuccess: true,
                }))

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    transferData(login) {
        return API_USERS.getAccountByUsername(login, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get account with id: " + result);

                this.dataInfo[0] = result.id;
                this.dataInfo[1] = result.role;
                this.dataInfo[2] = result.username;
                this.dataInfo[3] = result.password;

                console.log("role: "+ result.role);

                if(this.dataInfo[1] === "patient")
                    this.getPatient(this.dataInfo[2]);
                else if(this.dataInfo[1] === "caregiver")
                    this.getCaregiver(this.dataInfo[2]);
                else
                {
                    this.setState(() => ({
                        loginSuccess: true,
                    }))
                }


            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let login = {
            username: this.username,
            password: this.password
        };
        console.log("username is: " + this.username);
        console.log("password is: " + login.password);

        this.transferData(login.username);


    }




    render() {

        const { loginSuccess } = this.state;
        if (loginSuccess === true)
        {
            console.log(loginSuccess);

            if(this.dataInfo[1] === "patient") {

                localStorage.setItem('account', 'patient');
                //document.cookie = "patient";
                //encodeURIComponent(document.cookie);
                //this.state.patient = this.getPatient(this.dataInfo[2]);
                return (<Redirect to={{
                    pathname: '/patient-page',
                    state: {patient: this.state.patient}
                }}/>)

            } else if(this.dataInfo[1] === "caregiver") {

                localStorage.setItem('account', 'caregiver');
                 //document.cookie = "caregiver";

                return (<Redirect to={{
                    pathname: '/caregiver-page',
                    state: {caregiver: this.state.caregiver},
                }}/>)

            } else if(this.dataInfo[1] === "doctor") {
                localStorage.setItem('account', 'doctor');
                //document.cookie = "doctor";
                //encodeURIComponent(document.cookie);
                return (<Redirect to={{
                    pathname: '/doctor'
                }}/>)
            }


        }



        return (
            <div
                className="Login">

                        <label>Username</label>
                        <Input name='username' id='usernameField' placeholder={"Enter your username"}
                               onChange={(e) => {
                                   this.username = (e.target.value);}}
                               type="text"
                        />


                        <label>Password</label>
                        <Input name='password' id='passwordField' placeholder={"Enter your password"}
                               onChange={(e) => {
                                   this.password = (e.target.value);}}
                               type = "password"
                        />

                <div className="buttonLogin">
                    <Button block type={"submit"} onClick = {this.handleSubmit}>
                    Login
                    </Button>
                </div>

            </div>
        )
    }
}

export default Login;