import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "./api/caregiver-api"
import CaregiverTable from "./components/caregiver-table";
import CaregiverForm from "./components/caregiver-form";
import CaregiverUpdateForm from "./components/caregiver-update-form";
import * as API_USERS_A from "../patient/api/patient-api";
import CaregiverPatientsTable from "./components/caregiver-patients-table";
import PatientMedicalPlanForm from "../patient/components/patient-medical-plan-form";
import PatientUpdateForm from "../patient/components/patient-update-form";
import PatientForm from "../patient/components/patient-form";
import {Redirect} from "react-router-dom";



class CaregiverContainer extends React.Component {
dataInfo = [];
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloaded = this.reloaded.bind(this);
        this.reload = this.reload.bind(this);
        this.validate1 = false;
        this.validate2 = false;
        this.patientList = null;
        this.state = {
           // validate1: false,
           // validate2:false,
            patientsList : [],
            id : "",
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchCaregiver();
        this.fetchPatient();
    }

    fetchCaregiver() {
        return API_USERS.getCaregiver((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    fetchPatient() {
        return API_USERS_A.getPatients((result, status, err) => {

            if (result !== null && status === 200) {

                this.patientList = result;



            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleUpdateById(id) {
        return API_USERS.getCaregiverById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get caregiver with id: " + id);
                this.setState({
                    patientsList : result.listOfPatients,

                });

                this.dataInfo[0] = result.id;
                this.dataInfo[1] = result.name;
                this.dataInfo[2] = result.address;
                this.dataInfo[3] = result.birthdate;
                this.dataInfo[4] = result.gender;
                this.dataInfo[5] = result.username;
                this.dataInfo[6] = result.password;
                this.dataInfo[7] = result.listOfPatients;
                console.log("The patients are: "+ result.listOfPatients)


            }
            else console.log("failed with id: "+ id);



        });


    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
        this.validate1 = false;

            this.validate2 = false;
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCaregiver();
    }

    reloaded() {
        this.setState({
            isLoaded: false
        });
        this.fetchCaregiver();
    }

    handleById = (id) => {
        this.toggleForm();
        this.state.id = id;
        this.validUpdate();
        this.handleUpdateById(id);
        console.log("id: validate1 -> " + this.validate1 + " validate 2 " + this.validate2);


    }
    validUpdate(){
        this.validate1=true;
    }

    handlePatients = (id) => {

        API_USERS.getCaregiverById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get caregiver with id: " + id);
                this.setState({
                    patientsList: result.listOfPatients,

                });

                this.state.id = id;
                this.validate2 = true;
                this.toggleForm();

                this.validate1 = false;
            }
            else console.log("cevaaaaaaa");
        });
        console.log("patient are === "+this.state.patientsList);

        console.log("pat: validate1 -> " + this.validate1 + " validate 2 " + this.validate2);

    }

    render() {
        //const { patients } = this.state;
        if(localStorage.getItem('account') === "doctor") {
        return (
            <div>
                <CardHeader>
                    <strong> Caregiver Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Caregiver </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <CaregiverTable tableData = {this.state.tableData} idValue = {this.handleById} idCaregiver = {this.handlePatients} reloadHandler = { this.reloaded } />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> {(this.validate2)? "View Patients" : (this.validate1)? "Update Caregiver" : "Add Caregiver" }  </ModalHeader>
                    <ModalBody>
                        {(this.validate2)? <CaregiverPatientsTable patientsList = {this.state.patientsList}/>: (this.validate1)?<CaregiverUpdateForm reloadHandler={this.reload} id = {this.state.id} dataInfo = {this.dataInfo} patientList = {this.patientList} /> : <CaregiverForm reloadHandler={this.reload}/> }

                    </ModalBody>
                </Modal>

            </div>
        )
        }else {

            return (<Redirect to={{
                pathname: '/login'}}/>);
        }

    }
}


export default CaregiverContainer;
