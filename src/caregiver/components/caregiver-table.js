import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../../caregiver/api/caregiver-api";



class CaregiverTable extends React.Component {

    constructor(props) {
        super(props);
        this.reloadHandler = this.props.reloadHandler;
        this.valid = false;
        this.state = {
            tableData: this.props.tableData
        };
    }

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Username',
            accessor: 'username',
        },
        {
            Header: 'Password',
            accessor: 'password',
        },
        {
            Header: 'Delete',
            accessor: 'id',
            Cell:cell=>(<button className="deleteButton" onClick={()=> this.handleDelete(cell.value)}> Delete </button>)
        },
        {
            Header: 'Update',
            accessor: 'id',
            Cell:cell=>(<button className="updateButton" onClick={()=> this.handleById(cell.value)}> Update </button>)
        },
        {
            Header: 'View Patients',
            accessor: 'id',
            Cell:cell=>(<button className="updateButton" onClick={()=> this.handlePatients(cell.value)}> View Patients </button>)
        }
    ];

     filters = [
        {
            accessor: 'name',
        }
    ];

    handleDelete(id) {
        return API_USERS.deleteCaregiverById(id, (result, status) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted caregiver with id: " + result);
                this.reloadHandler();
            }
        });

    }

    handlePatients(id) {
        this.props.idCaregiver(id);
        return id;
    }

    handleById(id) {
        this.props.idValue(id);
        return id;
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.columns}
                search={this.filters}
                pageSize={5}
                onChange={this.handleById}
            />
        )
    }
}

export default CaregiverTable;