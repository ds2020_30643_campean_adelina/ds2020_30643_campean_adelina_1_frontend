import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    medication: '/medication'
};

function getMedication(callback) {
    let request = new Request(HOST.backend_api + endpoint.medication, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.medication + "/" + params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationByName(params, callback){
    let request = new Request(HOST.backend_api + endpoint.medication + "/find-user/" + params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteMedication(params, callback) {
    if(window.confirm('Are you sure?')) {
        let request = new Request(HOST.backend_api + endpoint.medication + "/" + params, {
            method: 'DELETE',

        });


        console.log("URL: " + request.url);
        RestApiClient.performRequest(request, callback);
    }
}

function insertMedication(user, callback){
    let request = new Request(HOST.backend_api + endpoint.medication , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedication(user, callback){
    let request = new Request(HOST.backend_api + endpoint.medication , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedication,
    getMedicationById,
    deleteMedication,
    insertMedication,
    updateMedication,
    getMedicationByName
};
